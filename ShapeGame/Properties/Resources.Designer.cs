﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ShapeGame.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ShapeGame.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream Applause_small {
            get {
                return ResourceManager.GetStream("Applause_small", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream BD010 {
            get {
                return ResourceManager.GetStream("BD010", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream BELLS_IN {
            get {
                return ResourceManager.GetStream("BELLS_IN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream BG_ORGAN {
            get {
                return ResourceManager.GetStream("BG_ORGAN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream CARIBBEA {
            get {
                return ResourceManager.GetStream("CARIBBEA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream clap {
            get {
                return ResourceManager.GetStream("clap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream CRASH_16 {
            get {
                return ResourceManager.GetStream("CRASH_16", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream CRYSTAL_ {
            get {
                return ResourceManager.GetStream("CRYSTAL_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream CYMBAL_I {
            get {
                return ResourceManager.GetStream("CYMBAL_I", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream DISCO_IN {
            get {
                return ResourceManager.GetStream("DISCO_IN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream EFFECTS19 {
            get {
                return ResourceManager.GetStream("EFFECTS19", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream EFFECTS21 {
            get {
                return ResourceManager.GetStream("EFFECTS21", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream ELEC__GU {
            get {
                return ResourceManager.GetStream("ELEC__GU", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream FDNG_GTR {
            get {
                return ResourceManager.GetStream("FDNG_GTR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream FLUTE {
            get {
                return ResourceManager.GetStream("FLUTE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream FUNKY_IN {
            get {
                return ResourceManager.GetStream("FUNKY_IN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream HARP_INT {
            get {
                return ResourceManager.GetStream("HARP_INT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream HARP_INT__1_ {
            get {
                return ResourceManager.GetStream("HARP_INT__1_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First try.
        /// </summary>
        public static string Hello {
            get {
                return ResourceManager.GetString("Hello", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream HI_HAT_O {
            get {
                return ResourceManager.GetStream("HI_HAT_O", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream HI_TECH2 {
            get {
                return ResourceManager.GetStream("HI_TECH2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream Hit_2 {
            get {
                return ResourceManager.GetStream("Hit_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST092 {
            get {
                return ResourceManager.GetStream("INST092", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST093 {
            get {
                return ResourceManager.GetStream("INST093", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST112 {
            get {
                return ResourceManager.GetStream("INST112", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST113 {
            get {
                return ResourceManager.GetStream("INST113", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST155 {
            get {
                return ResourceManager.GetStream("INST155", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST158 {
            get {
                return ResourceManager.GetStream("INST158", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST161 {
            get {
                return ResourceManager.GetStream("INST161", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST187 {
            get {
                return ResourceManager.GetStream("INST187", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST239 {
            get {
                return ResourceManager.GetStream("INST239", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST252 {
            get {
                return ResourceManager.GetStream("INST252", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST253 {
            get {
                return ResourceManager.GetStream("INST253", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST254 {
            get {
                return ResourceManager.GetStream("INST254", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST267 {
            get {
                return ResourceManager.GetStream("INST267", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST297 {
            get {
                return ResourceManager.GetStream("INST297", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream INST298 {
            get {
                return ResourceManager.GetStream("INST298", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream jetfly {
            get {
                return ResourceManager.GetStream("jetfly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream LO_INDIA {
            get {
                return ResourceManager.GetStream("LO_INDIA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream LUJON__F {
            get {
                return ResourceManager.GetStream("LUJON__F", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream MOD_GTR {
            get {
                return ResourceManager.GetStream("MOD_GTR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream NEAT_FTR {
            get {
                return ResourceManager.GetStream("NEAT_FTR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream PHASER_I {
            get {
                return ResourceManager.GetStream("PHASER_I", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream PONG {
            get {
                return ResourceManager.GetStream("PONG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream Pop_5 {
            get {
                return ResourceManager.GetStream("Pop_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream RIS_VOCL {
            get {
                return ResourceManager.GetStream("RIS_VOCL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream SAXAFONE {
            get {
                return ResourceManager.GetStream("SAXAFONE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        /// </summary>
        public static System.Drawing.Icon ShapeGame {
            get {
                object obj = ResourceManager.GetObject("ShapeGame", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream SITAR_1 {
            get {
                return ResourceManager.GetStream("SITAR_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream SLIDE_WH {
            get {
                return ResourceManager.GetStream("SLIDE_WH", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream SNARE_CR {
            get {
                return ResourceManager.GetStream("SNARE_CR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream SNARE_RO {
            get {
                return ResourceManager.GetStream("SNARE_RO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream SNGL_GNG {
            get {
                return ResourceManager.GetStream("SNGL_GNG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream SQUEAKYW {
            get {
                return ResourceManager.GetStream("SQUEAKYW", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream Squeeze {
            get {
                return ResourceManager.GetStream("Squeeze", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream STREAM_H {
            get {
                return ResourceManager.GetStream("STREAM_H", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream SYNTH_BE {
            get {
                return ResourceManager.GetStream("SYNTH_BE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream SYNTH_SO {
            get {
                return ResourceManager.GetStream("SYNTH_SO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream TANKER_H {
            get {
                return ResourceManager.GetStream("TANKER_H", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream TRUMPET {
            get {
                return ResourceManager.GetStream("TRUMPET", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream VIBRATIN {
            get {
                return ResourceManager.GetStream("VIBRATIN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Say something out loud to control the game!     Say colors and shapes like: &quot;Green Circles&quot;   &quot;Yellow Stars&quot;   &quot;Black Triangles&quot;   &quot;All Colors&quot;     Or say commands like: &quot;Speed Up&quot;   &quot;Slow Down&quot;   &quot;Bigger&quot;   &quot;Smaller&quot;   &quot;Stop&quot;   &quot;Go&quot;   &quot;Giant&quot;        Or say &quot;Reset&quot; to start over!.
        /// </summary>
        public static string Vocabulary {
            get {
                return ResourceManager.GetString("Vocabulary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream WHIRLER {
            get {
                return ResourceManager.GetStream("WHIRLER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.IO.UnmanagedMemoryStream similar to System.IO.MemoryStream.
        /// </summary>
        public static System.IO.UnmanagedMemoryStream WHIRLER__1_ {
            get {
                return ResourceManager.GetStream("WHIRLER__1_", resourceCulture);
            }
        }
    }
}
